class TestFeatureF {
    public static void main(String[] a) {
        System.out.println((new Test().f()));
    }
}

class Test {

    public int f() {
        int result;
        int count;
        boolean isTrue;
        boolean isFalse;
        isTrue = false;
        isFalse = true;
        result = 0;
        count = 1;

        if (isTrue || isFalse){
            count = count + 1;
        } else{
        count = 5;
      }
      return count;
    }

    public boolean g(int n) {
        System.out.println(n);
        return false;
    }

}
